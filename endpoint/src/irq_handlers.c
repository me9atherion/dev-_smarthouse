/*
 * irq_handlers.c
 *
 *  Created on: 14.03.2014
 *      Author: Admin
 */
#include "LPC11xx.h"
#include "printf.h"
#include "client_rfm.h"
#include "gpio.h"

void HardFault_Handler(void)
{
//	while(1){
//		uart_printf("HARDFAULT!!!\r\n");
//	}
	printf("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\nHARDFAULT!!!!!!!\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n");
//	while(1);
	NVIC_SystemReset();
}

void IntDefaultHandler(void)
{
//	while(1){
//		printf("DEFAULT INTERRUPT\r\n");
//	}
	printf("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\nDEFAULT INTERRUPT!!!!!!!\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n");
	NVIC_SystemReset();
}

void PIOINT1_IRQHandler(void){
//	printf("PIOINT1 Handler\r\n");
//	for(uint8_t i = 0; i < 12; i++){
//		if(GPIOIntStatus(1, i)){
//			printf("PIN 1_%u INTERRUPT\r\n", i);
//		}
//	}
	if(GPIOIntStatus(1, 9))
		rfm_int();
	//NVIC_ClearPendingIRQ(EINT1_IRQn);
}

void PIOINT3_IRQHandler(void){
	__disable_irq();
	printf("PIO_3 INT!\r\n");
	__enable_irq();
}


void SSP1_IRQHandler(void){
	printf("SSP Handler!!!!\r\n");
	LPC_SSP1->ICR = 0x03; /* clear interrupt */
}
