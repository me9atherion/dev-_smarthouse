/*
1 * mqtt_wrapper.h
 *
 *  Created on: 30.07.2014
 *      Author: Admin
 */

#ifndef MQTT_WRAPPER_H_
#define MQTT_WRAPPER_H_
#include <stdint.h>
//#include "lan.h"

/* MQTT Library wrapper
 * This is module that provides MQTT support. It uses libMQTT library
 * for protocol support and enc28j60 library for LAN support. Made to
 * simplify MQTT communication between device and broker.
 */

//#define SERVER inet_addr(192,168,0,105) //test
#define SERVER inet_addr(185,25,118,29) //digi24.com.ua
#define CLIENT_ID "digi24_1"
// XXX #define CLIENT_ID "blahblahblah"

//Callback argument structure. Please define subscribe callbacks as follow:
//	void subscribe_function(mqtt_sub_callback_t *data);
typedef struct{
	// Topic from witch message was received.
	char *topic;
	// Message body.
	char *msg;
}mqtt_sub_callback_t;

// For inner usage.
typedef struct{
	char topic[20];
	void (*callback)(mqtt_sub_callback_t*);
}mqtt_sub_t;

// MQTT Wrapper init function
void mw_init(void);

// Main polling function. Please call this function in loop to make things work.
void mw_poll(void);

// Publishing function.
// Args:
//	topic		- target topic
//	msg			- message to publish
void mw_publish(char *topic, char *msg);

// Subscribing function
// Args:
//	topic		- topic to subscribe, can use "+" in topic as mask
//	callb		- callback function that will receive subscribed messages
void mw_subscribe(char *topic, void (*callb)(mqtt_sub_callback_t*));

// Displays connection status.
void mw_status(void);

#endif /* MQTT_WRAPPER_H_ */
