/*
===============================================================================
 Name        : main.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/
#include <cr_section_macros.h>
#ifdef __USE_CMSIS
#include "LPC11xx.h"
#endif

#include "printf.h"
#include "pwr.h"
#include "leds.h"
#include "client_rfm.h"
#include "adc_utils.h"
#include "ext.h"
#include "temp.h"

#define SELF_ADDR 777
//#define DOOR
#define TEMP
//#define MOVE

#if defined (TEMP)
uint16_t addr = SELF_ADDR + 11000;
#elif defined (MOVE)
uint16_t addr = SELF_ADDR + 12000;
#elif defined (DOOR)
uint16_t addr = SELF_ADDR + 13000;
#endif


uint8_t event_wake = 0;
uint8_t retries = 0;

void tasks_init(){
	adc_init();
#if defined (TEMP)
	temp_init();
#endif
#if defined (DOOR) || defined (MOVE)
	ext_init();
#endif
}

uint8_t do_tasks(){
	static uint8_t send[32];
	static uint8_t temp_send = 1;
	uint8_t len = 0;
	//get VBATT
	//uint16_t vbatt = adc_vbat();lzre
	send[len++] = retries;
	//check for event wake
	if(event_wake){
		//printf("Event Wake\r\n");
#if defined (DOOR)
		send[len++] = 0x03;
		send[len++] = ext_int();
#elif defined (MOVE)
		send[len++] = 0x04;
		send[len++] = ext_int();
#endif
	}else{
#if defined (TEMP)
		if(temp_send){
			temp_send = 0;
			send[len++] = 0x02;
			send[len++] = temp()>>8;
			send[len++] = temp()&0xff;
		}else{
			temp_send = 1;
#endif
			send[len++] = 0x01; //VBATT indicator
			send[len++] = adc_vbat();
#if defined (TEMP)
		}
#endif
	}
	//send packet
	return rfc_guarant_send(send, len);
}

int main(void) {

	uint16_t to_sleep = 0;

	pwr_init();
	pwr_switch_to_12MHz();
	uart_init(CLK_12MHZ);
	led_init();

	printf("LPC1114 Endpoint\r\n");
	rfc_init(addr);
	tasks_init();
	asm_wait(1000);
	led_on(LED1);
	printf("Main loop\r\n");
	rfc_config();
	uint8_t ret;
	while(1){
		ret = do_tasks();
		if(ret){
			printf("Entering deepsleep\r\n");
			to_sleep = 100;
			retries = 0;
			event_wake = 0;
		}else if(retries++ < 10){
			printf("Tasks error, retry after 1 second\r\n");
			to_sleep = 10;
		}else{
			printf("Tasks error, retry error, deepsleep\r\n");
			retries = 0;
			to_sleep = 1000;
			event_wake = 0;
		}
		led_off(LED1);
		printf("\r\n");
		pwr_deepsleep(to_sleep);
		led_on(LED1);
		if(pwr_is_event_wake() && !event_wake)
			event_wake = 1;
		if(event_wake)
			printf("Event ");
		printf("Waked\r\n");
		adc_init();
	}
    return 0 ;
}
