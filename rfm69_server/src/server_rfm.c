
#include <string.h>
#include "LPC11xx.h"
#include "rfm.h"
#include "server_rfm.h"
#include "printf.h"
#include "gpio.h"
#include "leds.h"
#include "pwr.h"

//SOME PROBLEMS WITH VOLATIVITY IN RFM PART
//USING VOLATILE MACRO FROM LPC11xx.h


/* Описание типа данных для хранения пула уникальных идентификаторов привязанных
 * конечных точек и последовательности общения между ними (для определения перезапроса) */
typedef struct {
	uint16_t addr;		// Идентификатор
	uint8_t seq;		// Последовательность
} bind_table_t;


// RFM
rfm_data_t rfm_buffer_tx;			// Буфер передачи
rfm_data_t rfm_buffer_rx;			// Буфер приема
volatile uint8_t len;				// Длина последнего принятого и отфильтрованого пакета
volatile uint16_t to_resolve;		// Идентификатор очередного девайса, запросившего авторизацию

// MAC
volatile uint8_t self_pipe;
bind_table_t bind_table[MAX_BIND_DEVICES];



/*===================================================================================
 * Есть ли в таблице такой идентификатор? Возвращает 1 в случае наличия.
 *===================================================================================*/
static uint8_t is_in_table (uint16_t addr) {

	for(uint8_t i = 0; i < MAX_BIND_DEVICES; i++) {

		if(addr == bind_table[i].addr)
			return 1;
	}
	return 0;
}


/*===================================================================================
 * Добавление последовательности
 * Возвращает указатель на последовательность или NULL если нету места в пуле
 * для добавления нового идентификатора.
 *===================================================================================*/
static uint8_t* add_seq (uint16_t addr) {

	for(uint8_t i = 0; i < MAX_BIND_DEVICES; i++) {

		if(bind_table[i].addr == 0) {

			bind_table[i].addr = addr;
			return &(bind_table[i].seq);
		}
	}
	return NULL;
}


/*===================================================================================
 * Получение последовательности
 *===================================================================================*/
static uint8_t* get_seq (uint16_t addr) {

	for(uint8_t i = 0; i < MAX_BIND_DEVICES; i++) {

		if(addr == bind_table[i].addr) {
			return &(bind_table[i].seq);
		}
	}
	return NULL;
}



/*===================================================================================
 * Коллбек из прерывания по событию приема пакета
 *===================================================================================*/
static void recv_callback (rfm_data_t* packet) {

	uint16_t packet_device_id = 0;

	asm_wait(100);

	/* Выводим информацию о принятом пакете */
	printf("Incoming! pipe:%u, len:%u\r\n", packet->addr, packet->msg[0]);

	/* Проверяем поле длины пакета */
	if( (packet->msg[0] > (sizeof(rfm_buffer_rx.msg) - 1) ) || \
			(packet->msg[0] < 2) ) {
		/* Если поле длины пакета больше максимального размера секции сообщения минус 1 байт
		 * на само поле длины или размер сообщения меньше 2 (в пакете даже нету идентификатора),
		 * то выходим с обработчика а пакет отбрасывается */

		printf("Wrong packet size!\r\n");
		return;
	}

	/* Вычитываем уникальный идентификатор девайса, который инициировал
	 * передачу хабу */
	packet_device_id = (uint16_t) packet->msg[1];
	packet_device_id <<= 8;
	packet_device_id |= (uint16_t) packet->msg[2];

	printf("Device ID from packet: %u.\r\n", packet_device_id);

	/* Обрабатываем пакет */
	/* =========================================================================================*/
	if( (packet->addr == NETW_BROADCAST_ADDR) && (packet->msg[0]) == 2) {
		/* Пришел только идентификатор девайса по броадкасту. Нужно
		 * авторизовать девайс на сервере и послать устройству новый
		 * адрес пайпа */

		printf("Device with ID:%u requested authorization.\r\n", packet_device_id);

		if(is_in_table(packet_device_id)) {
			/* Девайс авторизован (есть в таблице) */

			printf("Device with ID: %u authorized. New pipe for device: %u.\r\n", packet_device_id, self_pipe);

			/* Копируем адрес и идентификатор с посылки  */
			memset(&rfm_buffer_tx, 0xFF, sizeof(rfm_buffer_tx));
			memcpy(&rfm_buffer_tx, packet, 4);
			rfm_buffer_tx.msg[0] = 3;					// Длина 3 байта (ИД + новый пайп)
			rfm_buffer_tx.msg[3] = self_pipe;		// Пайп для общения эндпоинта со своим хабом

			rfm_fast_send((uint8_t *) &rfm_buffer_tx);

			to_resolve = 0;

		} else {
			/* Посылка пришла от девайса который не авторизован (не привязан к
			 * этому хабу). Нужно резолвить его авторизацию */

			/* Ставим в очередь очередной ИД запросивший авторизацию */
			to_resolve = packet_device_id;
			printf("To resolve device ID:%u\r\n", to_resolve);
		}
		return;
	}

	/* Пришел новый пакет по пайпу хаба. */
	if(packet->addr == self_pipe) {

		printf("New packet. Seq. field: %u.\r\n", packet->msg[3]);

		/* Получаем информацию о последовательности предыдущего пакета, чтобы определить
		 * новый это пакет или старый */
		uint8_t *seq = get_seq(packet_device_id);

		if(seq == NULL) {
			/* Если в таблице идентификаторов конечных точек нет нашего девайса, но
			 * он по факту пытается общаться по пайпу своего хаба - значит надо его добавить в
			 * таблицу */
			printf("Adding device to bind table.\r\n");
			seq = add_seq(packet_device_id);
		}

		if(seq == NULL) {
			/* Если в пуле идентификаторов закончилось место - выходим */
			printf("Bind table is full! Packet ignored!\r\n");
			return;
		}

		/* На этом этапе получено значение последовательности с предыдущего пакета */

		uint8_t packet_seq_field = packet->msg[3];		// Забираем значение последовательности с входящего пакета

		/* Проверяем, новый это пакет от эндпоинта или старый (если подтверждение
		 * о получении пакета эндпоинт не получил, то попытается отправить перезапрос -
		 * значит пакет старый) */
		if( packet_seq_field <= *seq ) {

			/* Пакет считается новым. Можно его забирать */
			len = packet->msg[0];											// Забираем значение длины
			memcpy(&rfm_buffer_rx, packet, sizeof(rfm_buffer_rx));		// Копируем входящий пакет в буфер
			printf("New data aquired.\r\n");

			/* Копируем пайп и идентификатор с входящего пакета, меняем поле длины и отправляем
			 * конечному устройству подтверждение о доставке */
			memset(&rfm_buffer_tx, 0xFF, sizeof(rfm_buffer_tx));
			memcpy(&rfm_buffer_tx, packet, 4);
			rfm_buffer_tx.msg[0] = 2;
			rfm_fast_send((uint8_t *) &rfm_buffer_tx);
			printf("ACK sent.\r\n");

		} else {
			/* Выводим сообщение о том что пакет не прошел проверку
			 * последовательности */

			printf("Packet sequence fields missmatch: in new packet: %u, in prev.packet: %u!\r\n", \
					packet_seq_field, *seq);
			printf("No data acquired!\r\n");
		}
		/* Сохраняем поле последовательности предыдущего пакета */
		*seq = packet_seq_field;

		return;
	}

	printf("Packet refuzed!\r\n");
}


/*===================================================================================
 * Инит радиомодуля в режиме хаба (сервера)
 *===================================================================================*/
void rfs_init (uint16_t _self) {

	/* Очищаем буферы и локальные состояния */
	memset(&rfm_buffer_tx, 0xFF, sizeof(rfm_buffer_tx));
	memset(&rfm_buffer_rx, 0xFF, sizeof(rfm_buffer_rx));

	len = 0;
	to_resolve = 0;
	/* XXX Инит пайпа хаба.
	 * Пайп хаба должен быть уникальным для всех хабов в округе */
	self_pipe = (uint8_t) _self;

	for(uint8_t i = 0; i < MAX_BIND_DEVICES; i++) {
		bind_table[i].addr = 0;
		bind_table[i].seq = 0;
	}


	/* После инита радиомодуля разрешаются его прерывания,
	 * но по умолчанию он находится в спящем режиме */
	rfm_init();

	/* Определяем коллбек по событию приема пакета радиомодулем */
	rfm_set_rx_callback(recv_callback);

	/* Записываем идентификатор сети */
	rfm_set_sync(1, NETW_ID);

	/* После инита хаб должен принимать броадкастные сообщения и сообщения по своему пайпу */
	rfm_set_address(1, self_pipe, NETW_BROADCAST_ADDR);

	/* Радиомодуль после передачи будет в режиме приема */
	rfm_set_def_mode(RF_OPMODE_RECEIVER);

	/* Включаем приемник и слушаем */
	rfm_listen();
}


/*===================================================================================
 * Передача пакета клиенту с определенным идентификатором.
 * Пакет отправляется по пайпу хаба
 *===================================================================================*/
uint8_t rfs_send (uint16_t client_id, uint8_t *data, uint8_t len) {

	/* Готовим буфер */
	memset(&rfm_buffer_tx, 0xFF, sizeof(rfm_buffer_tx));

	/* Пайп хаба */
	rfm_buffer_tx.addr = self_pipe;

	/* Пишем идентификатор получаеля */
	rfm_buffer_tx.msg[1] = (uint8_t)(client_id >> 8);		// ID MSB
	rfm_buffer_tx.msg[2] = (uint8_t) client_id;			// ID LSB

	/* Длина пакета не должна быть больше чем секция сообщения минус байт длины и
	 * минус 2 байта идентификатора (всего 3 байта). Отсекаем лишние данные. */
	if(len > (sizeof(rfm_buffer_tx.msg) - 3))
		len = sizeof(rfm_buffer_tx.msg) - 3;

	rfm_buffer_tx.msg[0] = len + 2;				// ID + пользовательские данные
	memcpy(rfm_buffer_tx.msg + 3, data, len);

	/* Отправляем подготовленный пакет */
	rfm_send((uint8_t *) &rfm_buffer_tx);

	return 1;
}


/*===================================================================================
 * Запрос ACK
 *===================================================================================*/
void rfs_ask_ack (uint16_t addr) {
//this is in past, now we only add addresses to table
//	static uint8_t packet[10];
//	//make packet
//	packet[0] = 5;
//	packet[1] = addr>>8;
//	packet[2] = addr&0xff;
//	memcpy(packet+3, self, 3);
//	//switch to broadcast
//	rfm_set_tx_addr((uint8_t[]){0xe7, 0xe7, 0xe7});
//	//send
//	//rfm_write_payload(packet, 6);
//	//rfm_send();
//	rfm_fast_send(packet, 6);
//	//switch back
//	rfm_set_tx_addr(self);
	//uint8_t i;
	add_seq(addr);
}


/*===================================================================================
 * Получение очередного уникального идентификатора конечной точки,
 * которую надо авторизовать (проверить на авторизацию)
 *===================================================================================*/
uint16_t rfs_need_resolve (void) {

	uint16_t ret = to_resolve;

	rfm_disable_irq();
	to_resolve = 0;
	rfm_enable_irq();

	return ret;
}


/*===================================================================================
 * Прием пакета напротяжении определенного таймаута
 *===================================================================================*/
uint8_t rfs_recv (uint8_t *data, uint8_t timeout) {

	uint8_t ret_len = 0;

	if(timeout) {
		/* Ждем пакет */
		while(!len && timeout--) {
			asm_wait(1);
		}

		if(!timeout) {
			/* Вышло время ожидания */
			return 0;
		}
	}

	if(len) {

		/* Атомарно вычитываем и сбрасываем длину пакета, потому что во время
		 * копирования может прилететь очередной пакет и испортить данные.
		 * А если прилетит после копирования, то потеряется сам, потому что флаг длины нового пакета затрется  */
		rfm_disable_irq();

		ret_len = len;
		memcpy(data, rfm_buffer_rx.msg, ret_len);		// Забираем пакет без пайпа
		len = 0;

		rfm_enable_irq();

		return ret_len;
	}

	return ret_len;
}

/*===================================================================================
 * Обновление радиомодуля
 *===================================================================================*/
void rfs_refresh_rfm (void) {

	/* Переходим в спячку. Этим гарантируется завершение
	 * передачи пакета согласно алгоритму смены моды */
	rfm_nolisten();

	/* Чистим состояния */
	len = 0; to_resolve = 0;

	/* Стендбай после инита (временная дефолтная мода для повторной инициализации).
	 * * Учитываем что коллбек уже был установлен */
	rfm_set_def_mode(RF_OPMODE_STANDBY);

	/* Повторный инит радиомодуля */
	rfm_init();

	/* Записываем идентификатор сети */
	rfm_set_sync(1, NETW_ID);
	/* После инита хаб должен принимать броадкастные сообщения и сообщения по своему пайпу */
	rfm_set_address(1, self_pipe, NETW_BROADCAST_ADDR);

	/* Дефолтную моду обратно в прием */
	rfm_set_def_mode(RF_OPMODE_RECEIVER);

	/* Включаем прием */
	rfm_listen();
}


/*===================================================================================
 * Отладочная информация с конфигами радиомодуля
 *===================================================================================*/
void rfs_config (void) {
	/* Редирект на функцию получения статуса основных регистров в
	 * rfm.h */
	rfm_state();
}
