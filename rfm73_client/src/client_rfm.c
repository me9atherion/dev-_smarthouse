/*
 * simple_rfm.c
 *
 *  Created on: 18.07.2014
 *      Author: Admin
 */
#include <string.h>
#include "LPC11xx.h"
#include "gpio.h"
#include "rfm.h"
#include "printf.h"
#include "client_rfm.h"
#include "pwr.h"
#include "leds.h"

// RFM part
volatile uint8_t buffer[40];
volatile uint8_t len;

//MAC part
volatile uint16_t self;
volatile uint8_t pipe[3];
volatile uint8_t ack;

void recv_callback(rx_packet_t *packet){
	uint16_t _self = 0;
	printf("Incoming: pipe %u len %u ", packet->pipe, packet->packet[0]);
	if(packet->packet[0] > 32)
		return;
	_self = packet->packet[1];
	_self <<= 8;
	_self |= packet->packet[2];
	printf("self %u ", _self);
	if(self != _self)
		return;
	if(!(pipe[0])){
		//ask ack packet
		if(packet->packet[0] == 5){
			//exact hit!
			//taking pipe addr
			memcpy(pipe, packet->packet+3, 3);
			printf("0x%02x 0x%02x 0x%02x\r\n", pipe[0], pipe[1], pipe[2]);
			rfm_set_rx_pipes(1);
			rfm_set_pipe_addr(0, pipe);
			rfm_set_tx_addr(pipe);
			led_on(LED2);
		}
		return;
	}
	if(pipe[0]){
		if(packet->packet[0] == 2){
			//ack
			printf("ACK\r\n");
			ack = 1;
			return;
		}
		//regular data
		printf("Reg data\r\n");
		memcpy(buffer, packet->packet, 32);
		len = buffer[0];
		//led_blink(LED2);
		return;
	}
	//no ACK is sent by endpoint
}

void rfc_init(uint16_t _self){
	memset(buffer, 0x00, 40);
	len = 0;
	memset(pipe, 0x00, 3);
	self = _self;
	LPC_IOCON->PIO1_9 &= ~0x07;
	GPIOSetDir(1, 9, 0);
	GPIOSetValue(1, 9, 0);
	GPIOIntDisable(1, 9);
	GPIOSetInterrupt(1, 9, 1, 0, 0);
	NVIC_EnableIRQ(EINT1_IRQn);
	rfm_init();
	//rfm_set_tx_addr((uint8_t[]){0x01, 0x02, 0x03});
	rfm_set_rx_pipes(1);
	//rfm_set_pipe_addr(1, (uint8_t[]){0x01, 0x02, 0x03});
	rfm_set_rx_callback(recv_callback);
	rfm_set_channel(73);
	GPIOIntEnable(1, 9);
	led_off(LED2);
	rfm_listen();
}

uint8_t rfc_send(uint8_t *data, uint8_t len){
	static uint8_t ask[] = {0x02, 0x00, 0x00};
	static uint8_t packet[32];
	if(!(pipe[0])){
		//make ask
		ask[1] = self>>8;
		ask[2] = self&0xff;
		rfm_write_payload(ask, 3);
		rfm_send();
		return 0;
	}
	packet[0] = len+2;
	packet[1] = self>>8;
	packet[2] = self&0xff;
	memcpy(packet+3, data, len);
	len += 3;
	rfm_write_payload(packet, len);
	rfm_send();
	//led_blink(LED2);
	return 1;
}

uint8_t rfc_guarant_send(uint8_t *data, uint8_t len){
	static uint8_t in[33];
	uint8_t retry = 10;
	ack = 0;
	rfc_send(data, len);
	uint8_t wait = 10;
	while(!ack && wait--)
		asm_wait(1);
	return ack;
}

uint8_t rfc_recv(uint8_t *data, uint8_t timeout){
	uint8_t ret = 0;
	if(timeout){
		//waiting for packet
		//asm_wait_nonblock(timeout, &_timeout);
		while(!len && timeout--)
			asm_wait(1);
		if(!timeout){
			//nothing to receive
			return 0;
		}
	}
	if(len){
		//returnning callback packet
		memcpy(data, buffer, len);
		ret = len;
		len = 0;
		return ret;
	}
	return ret;
}

void rfc_config(){
	printf("RFM Config: ");
	for(uint8_t i = 0; i < 24; i++){
		printf("0x%02x ", rfm_read(i));
	}
	printf("\r\n");
}
