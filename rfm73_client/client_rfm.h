/*
 * simple_rfm.h
 *
 *  Created on: 18.07.2014
 *      Author: Admin
 */

#ifndef SIMPLE_RFM_H_
#define SIMPLE_RFM_H_
#include <stdint.h>

/* RFM Client
 * RFM73 client device library. Simplifies authentification and
 * data transmissions between devices. All connections are P2P,
 * with one listener and many nodes. Every time node initiates
 * transmission and can wait for ACK, or data. In this case device
 * are able to deepsleep in between transmissions.
 * Simple usage:
 *  - init
 *  - resend untill success (binding sequence)
 *  - regular send
 */

// RFM Init function. Sets device address.
// Args:
//	_self	- ID of remote device
void rfc_init(uint16_t _self);

// Simple send function.
// Args:
//	data	- pointer to data to send
//	len		- size of data (must be less than 29 bytes)
// Returns: 1 - on success send, 0 - on bind (sent bind packet, not data)
uint8_t rfc_send(uint8_t *data, uint8_t len);

// Guaranteed send function. Based on previous function, but but with
// acknowledge wait.
// Args:
//	data 	- data to send
//	len		- size of data to send (<29 bytes)
// Returns: 1 - on success guaranteed send, 0 - on fail or bind
uint8_t rfc_guarant_send(uint8_t *data, uint8_t len);

// Data receive function. Both blocking and non-blocking mode support.
// Will wait for data for some time if timeout not 0.
// Args:
//	data	- buffer to place received data
//	timeout	- waiting timeout (>0 - if no current received data available, function
//				will wait for data during timeout, 0 - or return size of ready bytes, even 0)
// Returns: amount of readden bytes
uint8_t rfc_recv(uint8_t *data, uint8_t timeout);

//RFM Interrupt function, for use in irc_handlers.c file. Don not call directly.
extern void rfm_int(void);

//Config testing function. Do not use. For debug only.
void rfc_config(void);

#endif /* SIMPLE_RFM_H_ */
