/*
===============================================================================
 Name        : main.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/
#include <cr_section_macros.h>

#ifdef __USE_CMSIS
#include "LPC11xx.h"
#endif

#include <stdint.h>
#include <string.h>
#include "pwr.h"
#include "leds.h"
#include "printf.h"
#include "counter.h"
//#include "lan.h"
//#include "mqtt_wrapper.h"
#include "server_rfm.h"
#include "gpio.h"
#include "ap_090.h"
#include "spi_mqtt.h"

#define DUMMY_PRINTF

#define SELF_ADDR 333

//void udp_packet(eth_frame_t *frame, uint16_t len){
//
//}
//void icmp_packet(eth_frame_t *frame){
//
//}
//uint8_t tcp_listen(uint8_t id, eth_frame_t *frame){
//	return 0;
//}
//void tcp_read(uint8_t id, eth_frame_t *frame, uint8_t re){
//
//}
//void tcp_write(uint8_t id, eth_frame_t *frame, uint16_t len){
//
//}
//void tcp_closed(uint8_t id, uint8_t hard){
//
//}

void sensor_subscribe(mqtt_sub_callback_t *data){
	printf("Got sensor data\r\n");
	printf("Topic: %s, message: %s\r\n", data->topic, data->msg);
	led_blink(LED1);
//	return;
	if(data->topic[6] == '2'){
		//got lightbulb
		uint16_t _br = 0;
		atoi(data->msg, &_br);
		_br %= 20;
		printf("Setting brightness to %u\r\n", _br);
#if defined (DUMMY_PRINTF)
		ap_set_brightness(2, (uint8_t)_br);
#endif
//		char _topic[30];
//		uart_sprintf(_topic, "V/%u/1", SELF_ADDR);
//		char _msg[10];
//		uart_sprintf(_msg, "%u", _br);
//		mw_publish(_topic, _msg);
	}
	if(data->topic[6] == '1'){
		uint16_t _on = 0;
		atoi(data->msg, &_on);
		printf("Set power %s\r\n", (_on ? "ON" : "OFF"));
		if(_on)
			_on = 19;
#if defined (DUMMY_PRINTF)
		ap_set_brightness(1, (uint8_t)_on);
#endif
	}

}

void hub_subscribe(mqtt_sub_callback_t *data){
	printf("Got hub data\r\n");
	sm_publish("H/test", "testmessage");
}

void radio_recv(){

	uint8_t buf[32];
	char topic[20];
	char data[20];
	uint16_t t16;
	uint8_t len = rfs_recv(buf, 0);
	if(len){
		printf("Got data ");
		led_blink(LED2);
		uint16_t addr = buf[1];
		addr <<= 8;
		addr |= buf[2];
		printf("from %u ", addr);
		printf("len %u: \r\n", buf[0]-2);
		uint8_t *p = buf+4; //data section start
		while(*p != 0xff){
			switch(*p){
			case 0x01: //battery stat
//#if defined (DUMMY_PRINTF)
//				printf("\tBatt: %u - ", *(p+1));
//				strcpy(topic, "B/333/13190");
//				strcpy(data, "100");
//#else
				sprintf(topic, "B/%u/%u", SELF_ADDR, addr);
				sprintf(data, "%u", *(p+1));
//#endif
				p += 2;
				break;
			case 0x02: //temperature
//#if defined (DUMMY_PRINTF)
//				strcpy(topic, "V/333/11190");
//				strcpy(data, "24.5");
//#else
				t16 = (*(p+1)<<8) | *(p+2);
				sprintf(data, "%u.%u", t16/100, t16%100);
				printf("\tTemperature: %s - ", data);
				sprintf(topic, "V/%u/%u", SELF_ADDR, addr);
//#endif
				p += 3;
				break;
			case 0x03: //door
				printf("\tDoor: %s - ", (*(p+1) ? "Closed" : "Opened"));
//#if defined (DUMMY_PRINTF)
//				strcpy(topic, "V/333/13190");
//				strcpy(data, "1");
//#else
				sprintf(topic, "V/%u/%u", SELF_ADDR, addr);
				sprintf(data, "%u", *(p+1));
//#endif
				p += 2;
				break;
			case 0x04: //move sensor
				printf("\tMove detected - ");
//#if defined (DUMMY_PRINTF)
//				strcpy(topic, "V/333/12190");
//				strcpy(data, "1");
//#else
				sprintf(topic, "V/%u/%u", SELF_ADDR, addr);
				sprintf(data, "%u", *(p+1));
//#endif
				p += 2;
				break;
			default:
				//printf("Error: Unused code: %u\r\n", *p);
				return;
			}
			sm_publish(topic, data);
			asm_wait(10);
		}

	}
	uint16_t resolv = rfs_need_resolve();
	if(resolv){
		printf("Resolving %u\r\n", resolv);
		rfs_ask_ack(resolv);
	}
}

int main(void) {

	pwr_init();
	pwr_switch_to_48MHz();
	uart_init(CLK_48MHZ);
	printf("LPC1114 GIT test\r\n");
	led_init();
	sm_init();
	rfs_init(SELF_ADDR);
	char topic[20];
	strcpy(topic, "S/333/+");
//	sprintf(topic, "S/%u/+", SELF_ADDR);
	sm_subscribe(topic, sensor_subscribe);
	strcpy(topic, "H/333/+");
//	sprintf(topic, "H/%u/+", SELF_ADDR);
	sm_subscribe(topic, hub_subscribe);
	printf("Main loop\r\n");
	volatile uint32_t i = 0;
    while(1) {
    	if(i == 0 || count_1khz() - i > 2000){
    		i = count_1khz();
//    		mw_status();
    		printf("RFM: %x\r\n", rfm_get_status());
//    		rfs_refresh_rfm();
    	}
    	sm_poll();
    	radio_recv();
    	led_poll();
    }
    return 0 ;
}
