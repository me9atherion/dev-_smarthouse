
#include "LPC11xx.h"
#include "rfm69w_spi.h"


/*=============================================================================
 * Инит интерфейса коммуникации с радиомодулем включая хардварный SPI0 и
 * порты общего назначения для девайса.
 *=============================================================================*/
void rfm69w_InitHWInterface() {

	uint8_t i = 8;
	volatile uint8_t temp;


	LPC_SYSCON->PRESETCTRL |= (1 << 0)|(1 << 2); /* disable reset */
	LPC_SYSCON->SYSAHBCLKCTRL |= (0x1<<11); /* enable SSP0 clock */
	LPC_SYSCON->SSP0CLKDIV = (SystemCoreClock/8000000) + 1;			/* XXX: Поменял делитель на автоматический подсчет. Максимальная частота
	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	   интерфейса модуля 10МГц, ограничил с запасом до 8МГц */
	LPC_IOCON->PIO0_2 &= ~0x07;
	//LPC_IOCON->PIO0_2 |= 0x01;		/* SPI SSEL */

	LPC_IOCON->SCK_LOC = 0x02;		/* SPI SCK on P0.6 */
	LPC_IOCON->PIO0_6 &= ~0x07;
	LPC_IOCON->PIO0_6 |= 0x02;		/* SPI SCK */
	LPC_IOCON->PIO0_8 &= ~0x07;
	LPC_IOCON->PIO0_8 |= 0x09;		/* SPI MISO */
	LPC_IOCON->PIO0_9 &= ~0x0F;
	LPC_IOCON->PIO0_9 |= 0x09;		/* SPI MOSI */

	LPC_SSP0->CR0 = 0x0707;			/* SPI 8bit len, SCR is 15 */
	LPC_SSP0->CR1 = 0x0;			/* master mode, disabled */
	LPC_SSP0->CPSR = 0x2;
	LPC_SSP0->IMSC = 0x0;			/* disable all interrupts */
	LPC_SSP0->CR1 |= (1 << 1); 		/* enable SPI controller */
	while(i--){
		temp = LPC_SSP0->DR;		// XXX: Не понятно для чего
	}
	//SSP_Init(0);
	//GPIOSetDir(0, 2, 1);
	LPC_GPIO0->DIR |= 1 << 2;
	//GPIOSetValue(0, 2, 1);
	LPC_GPIO0->MASKED_ACCESS[(1<<2)] = (1<<2);
	LPC_IOCON->PIO0_7 &= ~0x07;
	LPC_IOCON->PIO0_7 |= 0x08;
	//GPIOSetDir(0, 7, 1);			/* XXX: в rfm69w это уже пин ресета модуля */
	LPC_GPIO0->DIR |= 1 << 7;
	//GPIOSetValue(0, 7, 0);
	LPC_GPIO0->MASKED_ACCESS[(1<<7)] = 0;
}


/*=============================================================================
 * Деинициализация хардварного интерфейса
 *=============================================================================*/
void rfm69w_DeInitHWInterface (void) {

	/* Ресет на предварительно инициализированные SPI интерфейсы */
	LPC_SYSCON->PRESETCTRL &= ~( (1 << 0) | (1 << 2) );

	/* Отключаем тактирование SSP0 */
	LPC_SYSCON->SYSAHBCLKCTRL &= ~(1<<11);
}


/*=============================================================================
 * Передача или прием байта в SPI0. Возвращает соответственно принятый байт
 * и принимает байт на передачу
 *=============================================================================*/
uint8_t rfm69w_SPIRxTx(uint8_t txbyte) {
	while (!(LPC_SSP0->SR & SSPSR_TFE));
	LPC_SSP0->DR = (uint16_t)txbyte;
	while (LPC_SSP0->SR & SSPSR_BSY);
	//while ( (LPC_SSP0->SR & (SSPSR_BSY|SSPSR_RNE)) != SSPSR_RNE );
	return (uint8_t)(LPC_SSP0->DR & 0xff);
	//return SSP0_TxRx(txbyte);
}


/*=============================================================================
 * Инит коммуникации с радиомодулем по SPI - ChipSeleсt в 0
 *=============================================================================*/
void rfm69w_SPISelect() {
	//GPIOSetValue(0, 2, 0);
	LPC_GPIO0->MASKED_ACCESS[(1<<2)] = 0;
}


/*=============================================================================
 * Окончание коммуникации с радиомодулем по SPI - ChipSeleсt в 1
 *=============================================================================*/
void rfm69w_SPIRelease() {
	//GPIOSetValue(0, 2, 1);
	LPC_GPIO0->MASKED_ACCESS[(1<<2)] = (1<<2);
}


/*=============================================================================
 * Высокий уровень на ресете (активация сброса)
 *=============================================================================*/
void rfm69w_ModuleRstHigh() {
	//GPIOSetValue(0, 7, 1);
	LPC_GPIO0->MASKED_ACCESS[(1 << 7)] = 1 << 7;
}


/*=============================================================================
 * Низкий уровень на ресете (деактивация сброса)
 *=============================================================================*/
void rfm69w_ModuleRstLow() {
	//GPIOSetValue(0, 7, 0);
	LPC_GPIO0->MASKED_ACCESS[(1 << 7)] = 0;
}
