/* Библиотека для управления радиомодулем RFM69W.
 * Эта реализация предполагает пакетный режим приемопередачи с фиксированной длиной пакета.
 * Независимо от количества полезных данных длина всего пакета должна превышать количество
 * полезных данных минимум на 1 байт для хранения адреса (определяет получателя посылки):
 * PayloadLenght >= UserDataLen + AddrLen (1 byte).
 * Суммарная длина пакта в свою очередь не должна превышать длины буфера FIFO радиомодуля:
 * PayloadLenght =< 66 */


#ifndef RFM_H_
#define RFM_H_

#include <stdint.h>
#include "rfm69w_registers.h"


/* Длина пейлоада в 1-м пакете, который передает или принимает
 * радиомодуль (вместе с байтом адреса). Длина одной принимаемой или передаваемой
 * посылки будет всегда равна этому значению  */
#define RFM69_PAYLOAD_LEN			32


/* Структура пакета RFM69 */
typedef struct __attribute__ ((__packed__)) {
	uint8_t addr;							// Адресс получателя в посылке (пайп)
	uint8_t msg[RFM69_PAYLOAD_LEN - 1];		// Полезная нагрузка пакета (само сообщение)
} rfm_data_t;


/* Функционал библиотеки */
void rfm_disable_irq (void);							// Запрет прерываний от радиомодуля
void rfm_enable_irq (void);							// Разрешение прерываний от радиомодуля
void rfm_init (void);									// Инит радиомодуля
int rfm_rssi (void);									// Получение значения RSSI
void rfm_set_mode(uint8_t mode);						// Установка новой моды
void rfm_listen(void);								// Модуль в прием
void rfm_nolisten(void);								// Модуль в стендбай
void rfm_set_address(uint8_t enabled, \
		uint8_t node_addr, \
		uint8_t brdcst_addr);							// Установка адреса ноды и броадкаста
void rfm_set_sync(uint8_t enabled, uint32_t value);	// Установка синхронизационной последовательности
void rfm_set_def_mode(uint8_t def_mode);				// Установка дефолтной моды (радомодуль переходит туда после передачи)
void rfm_send(uint8_t *buf);							// Отправить пакет через радиомодуль
void rfm_fast_send(uint8_t *buf);						// Быстрая отправка пакета через радиомодуль
void rfm_set_rx_callback(void (*callb)(rfm_data_t*));		// Определение функции обратного вызова
void rfm_state (void);								// Статус
void rfm_int(void);									// Функция обработчик прерывания от радиомодуля

#endif /* RFM_H_ */
