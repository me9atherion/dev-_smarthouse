
#ifndef RFM69Config_h
#define RFM69Config_h


#include <stdint.h>
/* Используем описания из оригинальной библиотеки LowPowerLabs */
#include "rfm69w_registers.h"
#include "rfm.h"


/* Массив конфигурационных слов для инициализации радиомодуля в формате
 * регистр (старший байт) - значение (младший байт). Основа взята из ардуиновской библиотеки от LowPowerLabs
 * https://github.com/LowPowerLab/RFM69
 *
 * Для радиомодулей, рассчитаных на частоту 433МГц.
 */
const uint16_t RFM69W_ConfigTbl[] = {

		/* Переводим модуль в спящий режим для экономи батареи. Автоматический секвенсор не запрещаем,
		 * режим периодичного прослушивания эфира не включаем */
		(REG_OPMODE << 8) | RF_OPMODE_SEQUENCER_ON | RF_OPMODE_LISTEN_OFF | RF_OPMODE_SLEEP,
		/* Пакетный режим, FSK модуляция, без шейпинга */
		(REG_DATAMODUL << 8) | RF_DATAMODUL_DATAMODE_PACKET | RF_DATAMODUL_MODULATIONTYPE_FSK | RF_DATAMODUL_MODULATIONSHAPING_00,

		/* Битрейт */
		(REG_BITRATELSB << 8) | RF_BITRATELSB_9600,		// 9600 бод
		(REG_BITRATEMSB << 8) | RF_BITRATEMSB_9600,

		/* Девиация */
		(REG_FDEVMSB << 8) | RF_FDEVMSB_50000,	// Default:5khz, (FDEV + BitRate/2 <= 500Khz)
		(REG_FDEVLSB << 8) | RF_FDEVLSB_50000,

		/* Частота */
		(REG_FRFMSB << 8) | RF_FRFMSB_433,		// 433 MHz
		(REG_FRFMID << 8) | RF_FRFMID_433,
		(REG_FRFLSB << 8) | RF_FRFLSB_433,

		/* Мощность и токовая защита */
		// looks like PA1 and PA2 are not implemented on RFM69W, hence the max output power is 13dBm
		// +17dBm and +20dBm are possible on RFM69HW
		// +13dBm formula: Pout=-18+OutputPower (with PA0 or PA1**)
		// +17dBm formula: Pout=-14+OutputPower (with PA1 and PA2)**
		// +20dBm formula: Pout=-11+OutputPower (with PA1 and PA2)** and high power PA settings (section 3.3.7 in datasheet)
		(REG_PALEVEL << 8) | RF_PALEVEL_PA0_ON | RF_PALEVEL_PA1_OFF | RF_PALEVEL_PA2_OFF | RF_PALEVEL_OUTPUTPOWER_11111,
		// (REG_OCP << 8) | RF_OCP_ON | RF_OCP_TRIM_95, //over current protection (default is 95mA)

		/* Полоса приемника */
		// RXBW defaults are: (REG_RXBW << 8) | RF_RXBW_DCCFREQ_010 | RF_RXBW_MANT_24 | RF_RXBW_EXP_5 (RxBw: 10.4khz)
		(REG_RXBW << 8) | RF_RXBW_DCCFREQ_010 | RF_RXBW_MANT_16 | RF_RXBW_EXP_2, // (BitRate < 2 * RxBw)
		// for BR-19200: (REG_RXBW << 8) | RF_RXBW_DCCFREQ_010 | RF_RXBW_MANT_24 | RF_RXBW_EXP_3,

		/* Настройка выхода прерываня */
		// Прерывание на выход DI0 в слипе в любом случае не генерирутся, по этому
		// после записи этого конфига внезапного прерывания быть не должно
		(REG_DIOMAPPING1 << 8) | RF_DIOMAPPING1_DIO0_01, // DIO0 - [Rx-PayloadReady], [Tx-TxReady]

		/* Порог RSSI */
		(REG_RSSITHRESH << 8) | 220, // must be set to dBm = (-Sensitivity / 2) - default is 0xE4=228 so -114dBm

		/* Преамбула */
		(REG_PREAMBLELSB << 8) | 6, // 6 байт преамбулы всместо 3-х

		/* Синхронизация */
		// Настраивается снаружи
		// (REG_SYNCCONFIG << 8) | RF_SYNC_ON | RF_SYNC_FIFOFILL_AUTO | RF_SYNC_SIZE_4 | RF_SYNC_TOL_0,

		/* Настройка пакетов */
		// (REG_PACKETCONFIG1 << 8) | RF_PACKET1_FORMAT_VARIABLE | RF_PACKET1_DCFREE_OFF |
		//		RF_PACKET1_CRC_ON | RF_PACKET1_CRCAUTOCLEAR_ON | RF_PACKET1_ADRSFILTERING_OFF,	// Переменная длина пакета
		// (REG_PACKETCONFIG1 << 8) | RF_PACKET1_FORMAT_FIXED | RF_PACKET1_DCFREE_OFF |
		//		RF_PACKET1_CRC_ON | RF_PACKET1_CRCAUTOCLEAR_ON | RF_PACKET1_ADRSFILTERING_OFF,	// Постоянная длина пакета

		(REG_PACKETCONFIG1 << 8) | RF_PACKET1_FORMAT_FIXED | RF_PACKET1_DCFREE_MANCHESTER | \
				RF_PACKET1_CRC_ON | RF_PACKET1_CRCAUTOCLEAR_ON | RF_PACKET1_ADRSFILTERING_OFF,	// Постоянная длина пакета + отбрасывание битых пакетов по CRC + манчестер *
		// * Применение манчестерскго кодирования позволило устранить потерю пакетов из за неправильной контрольной суммы
		// Полезная нагрузка обычно мала по сравнению с общей длиной пакета, постоянная составляющая скорей всего была подвержена
		// помехам и отбрасывалась проверкой контрольной суммы

		(REG_PACKETCONFIG2 << 8) | RF_PACKET2_RXRESTARTDELAY_2BITS | RF_PACKET2_AUTORXRESTART_ON | RF_PACKET2_AES_OFF, //RXRESTARTDELAY must match transmitter PA ramp-down time (bitrate dependent)
		// for BR-19200: REG_PACKETCONFIG2 | RF_PACKET2_RXRESTARTDELAY_NONE | RF_PACKET2_AUTORXRESTART_ON | RF_PACKET2_AES_OFF, //RXRESTARTDELAY must match transmitter PA ramp-down time (bitrate dependent)

		/* Пейлоад */
		(REG_PAYLOADLENGTH << 8) | RFM69_PAYLOAD_LEN, 	// in variable length mode: the max frame size, not used in TX

		/* Адреса ноды и броадкаста */
		// (REG_NODEADRS << 8) | nodeID,
		// (REG_BROADCASTADRS << 8) | broadcast,

		/* Настройки FIFO*/
		(REG_FIFOTHRESH << 8) | RF_FIFOTHRESH_TXSTART_FIFONOTEMPTY | RF_FIFOTHRESH_VALUE, //TX on FIFO not empty

		/* DAGC */
		// (REG_TESTDAGC << 8) | RF_DAGC_CONTINUOUS, // run DAGC continuously in RX mode
		(REG_TESTDAGC << 8) | RF_DAGC_IMPROVED_LOWBETA0 // run DAGC continuously in RX mode, recommended default for AfcLowBetaOn=0
};

#endif
