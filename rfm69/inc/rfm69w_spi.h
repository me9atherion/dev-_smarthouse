/* Низкоуровневое управление радиомодулем RFM69W.
 * Используем интерфейс SPI0 и дополнительные пины общего назначения для управления потоком данных и
 * прерываниями модуля */

#include <stdint.h>


#ifndef SPI_NRF_H_
#define SPI_NRF_H_

#ifndef __SSP_COMMON__
#define __SSP_COMMON__


/* SSP Status register */
#define SSPSR_TFE       (0x1<<0)
#define SSPSR_TNF       (0x1<<1)
#define SSPSR_RNE       (0x1<<2)
#define SSPSR_RFF       (0x1<<3)
#define SSPSR_BSY       (0x1<<4)

/* SSP CR0 register */
#define SSPCR0_DSS      (0x1<<0)
#define SSPCR0_FRF      (0x1<<4)
#define SSPCR0_SPO      (0x1<<6)
#define SSPCR0_SPH      (0x1<<7)
#define SSPCR0_SCR      (0x1<<8)

/* SSP CR1 register */
#define SSPCR1_LBM      (0x1<<0)
#define SSPCR1_SSE      (0x1<<1)
#define SSPCR1_MS       (0x1<<2)
#define SSPCR1_SOD      (0x1<<3)

/* SSP Interrupt Mask Set/Clear register */
#define SSPIMSC_RORIM   (0x1<<0)
#define SSPIMSC_RTIM    (0x1<<1)
#define SSPIMSC_RXIM    (0x1<<2)
#define SSPIMSC_TXIM    (0x1<<3)

/* SSP0 Interrupt Status register */
#define SSPRIS_RORRIS   (0x1<<0)
#define SSPRIS_RTRIS    (0x1<<1)
#define SSPRIS_RXRIS    (0x1<<2)
#define SSPRIS_TXRIS    (0x1<<3)

/* SSP0 Masked Interrupt register */
#define SSPMIS_RORMIS   (0x1<<0)
#define SSPMIS_RTMIS    (0x1<<1)
#define SSPMIS_RXMIS    (0x1<<2)
#define SSPMIS_TXMIS    (0x1<<3)

/* SSP0 Interrupt clear register */
#define SSPICR_RORIC    (0x1<<0)
#define SSPICR_RTIC     (0x1<<1)

#endif /* __SSP_COMMON__ */


/* Инициализация низкоуровнего аппаратного интерфейса коммуникации с радиомодулем.
 * Вызывать до начала работы с радиомодулем */
void rfm69w_InitHWInterface(void);

/* Деинициализация хардварного интерфейса радиомодуля */
void rfm69w_DeInitHWInterface (void);

/* Передача/прием 1-го байта по SPI интерфейсу */
uint8_t rfm69w_SPIRxTx (uint8_t txbyte);

/* Инициализация передачи по SPI в радиомодуль */
void rfm69w_SPISelect (void);

/* Конец передачи по SPI в радиомодуль */
void rfm69w_SPIRelease (void);

/* Вывод ресета радиомодуля в 1 - активация сброса */
void rfm69w_ModuleRstHigh (void);

/* Вывод ресета радиомодуля в 0 - запуск */
void rfm69w_ModuleRstLow (void);

#endif /* SPI_NRF_H_ */
