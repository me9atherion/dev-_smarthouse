#include <string.h>

#include "uart_retransmit.h"
#include "rfm.h"
#include "uart.h"
#include "sys_tick.h"
#include "gpio.h"
#include "leds.h"


/* Внешние статусы индикаторов для отработки в основном цикле */
extern uint8_t tx_led_turned_on;


uint32_t uartRx_to_radioTx_poll (void) {


	static uint8_t buf [RFM69_PAYLOAD_LEN];				// Буфер
	static uint8_t buf_ptr = 0;						// Указатель буфера
	static uint32_t prev_ticks = 0;					// Тики
	static enum {
		INITIAL = 0,
		WAIT_FOR_NEXT_BYTE = 1,
	} rx_state = INITIAL;				// Состояние

	uint16_t byte;						// Принятый байт


	/* Читаем байт */
	byte = UARTRead();

	/* Читаем байт заново если байт невалидный и статус приема начальный */
	if( byte < 0x0100 && rx_state == INITIAL ) return 0;

	switch(rx_state) {

	/* Начальное состояние приемника */
	case INITIAL:

		if(byte > 0xFF) {
			/* Байт валидный */

			/* Пишем байт в буфер и получаем
			 * текущее значение тиков */
			buf[buf_ptr] = (uint8_t) byte;
			buf_ptr++;

			prev_ticks = systickGetTicks();

			/* Состояние меняем на ожидание следующего байта
			 * в пакете */
			rx_state = WAIT_FOR_NEXT_BYTE;
		};
		break;



	case WAIT_FOR_NEXT_BYTE:

		if(byte > 0xFF) {
			/* Байт валидный */

			/* Пишем байт в буфер и получаем
			 * текущее значение тиков */
			buf[buf_ptr] = (uint8_t) byte;
			buf_ptr++;

			/* Обновляем тики */
			prev_ticks = systickGetTicks();

			/* Проверяем на переполнение размера пакета */
			if(buf_ptr >= RFM69_PAYLOAD_LEN) {

				/* Передаем пакет */
				rfm_send(buf);

				led_on(LED1);				// Зажигаем при передаче в радиоканал
				tx_led_turned_on = 1;		// Флаг зажженного индикатора передачи

				/* Исходное состояние */
				buf_ptr = 0;
				rx_state = INITIAL;

				return 1;
			}

		} else {
			/* Байт невалидный */

			/* Проверяем предыдущее значение тиков */
			if(systickCalculateDelay_ms(prev_ticks) >= NEXT_BYTE_TIMEOUT_MS) {
				/* Таймаут на следующий байт вышел. Нужно передавать пакет
				 * и возвращать исходное состояние ретранслятора байт */

				/* Заполняем нулями незанятую область посылки */
				if(buf_ptr < RFM69_PAYLOAD_LEN) {
					memset(&buf[buf_ptr], 0xFF, RFM69_PAYLOAD_LEN - buf_ptr);
				}

				/* Передаем пакет */
				rfm_send(buf);

				led_on(LED1);				// Зажигаем при передаче в радиоканал
				tx_led_turned_on = 1;		// Флаг зажженного индикатора передачи

				/* Исходное состояние */
				buf_ptr = 0;
				rx_state = INITIAL;

				return 1;
			};
		};
		break;

	};

	return 0;
}

