
#ifndef SYS_TICK_H_
#define SYS_TICK_H_

/* Библиотечка для формирования задержек с помощью системного таймера */


/* Компромисс между нагрузкой на ядро (частота обработки прерывания от системного таймера) и
 * разрешающей способностью таймера. По этой константе инициализируется модуль и вычисляются
 * задержки в милисекундах */
#define MILIS_IN_TICK		1		// Сколько миллисекунд в одном тике системного таймера


/* Прототипы */
uint32_t systickInit (void);
void systickDeInit (void);
uint32_t systickGetTicks(void);
uint32_t systickCalculateDelay_ms (uint32_t prev_ticks);
void systickWait_ms (uint32_t milis);

#endif /* SYS_TICK_H_ */
