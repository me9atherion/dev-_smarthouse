#include <stdint.h>
#include "LPC11xx.h"
#include "sys_tick.h"
//#include "atomic.h"


/* Счетчик тиков */
volatile static uint32_t systickTicks = 0;

/* Флаг инициализации модуля. Если таймер инициализирован последующие
 * инициализиции не имеют силы - все остается по прежнему */
volatile static uint8_t sys_tick_initialized = 0;



/*=============================================================================
 * Отработка прерывания системного таймера
 *=============================================================================*/
void SysTick_Handler (void) {

	/* Просто считаем тики */
	systickTicks++;
}


/*=============================================================================
 * Настройка системного таймера на генерацию его прерываний с интервалом,
 * определенным настройками в заголовке этого модуля. Этот интервал будет
 * использоваться для расчета задержек времени функциями модуля.
 * Возвращает 1 в случае успешной инициализации и 0 в случае провала
 *=============================================================================*/
uint32_t systickInit (void) {

	register uint32_t reload_value;


	/* Повторная инициализация не требуется если до этого все уже инициализировано*/
	if(sys_tick_initialized) return 1;

	/* Вычисляем значение для передачи в системную функцию настройки
	 * системного таймера. С этим значением прерывания по этому таймеру
	 * будут происходить с заданным интервалом в милисекундах*/
	reload_value = (SystemCoreClock * MILIS_IN_TICK) / 1000;

	/* Запускаем таймер системной функцией */
	if(SysTick_Config(reload_value) == 0) {
		sys_tick_initialized = 1;
		return 1;
	}

	/* Если добрались сюда - то систик
	 * не инициализировался */
	return 0;
}


/*=============================================================================
 * Отключает системный таймер и переводит его регистры в исходное состояние
 *=============================================================================*/
void systickDeInit (void) {

	SysTick->CTRL = 0;
	SysTick->LOAD = 0;
	SysTick->VAL = 0;
	sys_tick_initialized = 0;
}


/*=============================================================================
 * Возвращает значение счетчика тиков в текущий момент времени
 *=============================================================================*/
__attribute__ ((always_inline)) inline uint32_t systickGetTicks (void) {

	return systickTicks;

	/* *** Добавил 06.09 - атомарный вариант */
//	register uint32_t ticks_to_return;
//
//	ATOMIC_BLOCK_RESTORATE {
//		ticks_to_return = systickTicks;
//	}
//
//	return ticks_to_return;
}


/*=============================================================================
 * Функция возвращает значение времени в миллисекундах на основе значения
 * счетчика тиков, взятого перед выполнением этой функции
 *=============================================================================*/
uint32_t systickCalculateDelay_ms (uint32_t prev_ticks) {

	/* Текущее значение тиков */
	register uint32_t current_ticks = systickGetTicks();

	return (current_ticks - prev_ticks) * MILIS_IN_TICK;
}

/*=============================================================================
 * Задержка с помощью системного таймера
 *=============================================================================*/
void systickWait_ms (uint32_t milis) {

	register uint32_t initial_ticks = systickGetTicks();

	/* Ждем пока не насчитаем нужное количество тиков */
	while (systickCalculateDelay_ms(initial_ticks) < milis);
}
