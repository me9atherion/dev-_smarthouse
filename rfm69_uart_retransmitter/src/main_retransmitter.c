
/* Ретранслятор для передачи данных по радиоканалу через UART.
 * Радиомодуля - RFM69W. Режим постоянной длины пакета
 * Осуществляет прозрачную связь по каналу UART между радиомодулями.
 * Также полезно будет использовать как слушалку радиоканала для отладки девайсов
 * на этих радиомодулях */


#ifdef __USE_CMSIS
#include "LPC11xx.h"
#endif

#include <cr_section_macros.h>

#include <stdint.h>

#include "leds.h"
#include "gpio.h"
#include "sys_tick.h"
#include "uart.h"
#include "uart_retransmit.h"
#include "rfm.h"


/* Задержка на блинк светодиода-индикатора состояния Tx или Rx */
#define LED_BLINK_DELAY_MS		30

/* Sync для интересующей нас сети */
#define NETW_ID				0xC55A4B2D

/* Определяет отправлять ли принятый пакет назад после приема */
//#define PACKET_ECHO



/* Флаги перевода светодиодов во включенный режим */
uint8_t tx_led_turned_on = 0, rx_led_turned_on = 0;
/* Хранилища тиков для вычисления времени */
uint32_t tx_led_prev_ticks = 0, rx_led_prev_ticks = 0;

/* Для задержки */
volatile const uint32_t ms_cycles = 12500; // 3125 for 12MHz, 12500 for 48MHz, 80 for 8kHz



/* Функции требуемые библиотеками */
void asm_wait(uint16_t _10_ms) {
	uint16_t n;
	while(_10_ms--){
		n = ms_cycles;
		while(n--){
			__NOP();
		}
	}
}

void printf(const char *format, ...) {
	/* Заглушка для принта */
	__NOP();
}
/* ----------------------------------------------------------  */



/*===================================================================================
 * Обработчик прерывания от радиомодуля
 *===================================================================================*/
void PIOINT1_IRQHandler(void){

	if(GPIOIntStatus(1, 9))
		rfm_int();
}


/*===================================================================================
 * Обработчик события приема пакета
 *===================================================================================*/
void radioRx_to_uartTx (void * packet) {

	/* По событию приема зажигаем светодиодик */
	led_on(LED2);
	rx_led_turned_on = 1;

	UARTSend((uint8_t *) packet, RFM69_PAYLOAD_LEN);

#ifdef PACKET_ECHO
	rfm_fast_send((uint8_t *) packet);
#endif

}



/*===================================================================================
 * main()
 *===================================================================================*/
int main(void) {

	/* Инит */
	SystemCoreClockUpdate();

	/* Систик */
	systickInit();
	systickWait_ms(500);

	UARTInit(115200);
	UARTSend((uint8_t *)"RFM69 Retransmitter\r\n", 21);

	/* Порты */
	led_init();

	/* Инитим радиомодуль */
	rfm_init();

	/* Коллбек */
	rfm_set_rx_callback((void *)radioRx_to_uartTx);
	/* Дефолтная мода */
	rfm_set_def_mode(RF_OPMODE_RECEIVER);
	/* Настраиваем нетворк */
	rfm_set_sync(1, NETW_ID);
	/* Сбрасываем фильтрацию по ноде и броадкасту */
	rfm_set_address(0, 0x00, 0x00);
	/* Слушаем эфир */
	rfm_listen();


	for(;;) {

		/* Форвардер UART -> RFM69W */
		uartRx_to_radioTx_poll();


		/*** Гашение светодиодов ***/
		/* Tx светодиод */
		if(tx_led_turned_on) {
			/* Светодиод включился. После таймаута надо выключить */

			/* Сохраняем отметку времени и снимаем флаг зажженного светодиода */
			tx_led_prev_ticks = systickGetTicks();
			tx_led_turned_on = 0;

		} else {
			/* Светодиод нужно выключить по истечении времени */

			if(systickCalculateDelay_ms(tx_led_prev_ticks) >= LED_BLINK_DELAY_MS) {
				led_off(LED1);
			}
		}

		/* Rx светодиод */
		if(rx_led_turned_on) {
			/* Светодиод включился. После таймаута надо выключить */

			/* Сохраняем отметку времени и снимаем флаг зажженного светодиода */
			rx_led_prev_ticks = systickGetTicks();
			rx_led_turned_on = 0;

		} else {
			/* Светодиод нужно выключить по истечении времени */

			if(systickCalculateDelay_ms(rx_led_prev_ticks) >= LED_BLINK_DELAY_MS) {
				led_off(LED2);
			}
		}

	}

    return 0 ;
}
