
#include "stdint.h"


#ifndef UART_RETRANSMIT_H_
#define UART_RETRANSMIT_H_

/* Таймаут в миллисекундах, при котором считается что хост прислал
 * очередной пакет для отправки. После этого пакет отправляется в радиомодуль
 * и передается по радиоканалу.
 * Тем не менее, если хост не переставал передавать данные, а максимальная длина буфера
 * радиомодуля исчерпана - пакет передается принудительно */
#define NEXT_BYTE_TIMEOUT_MS	5

/* Настройки маски сети (синхронизационной последовательности) и максимальной
 * длины пакета находятся в "rfm69w.h" */


uint32_t uartRx_to_radioTx_poll (void);		// С хоста в радиоканал
void radioRx_to_uartTx_Poll (void);			// С радиоканала в хост


#endif /* UART_RETRANSMIT_H_ */
