/*
 * printf_uart.c
 *
 *  Created on: Nov 15, 2013
 *      Author: manitou
 */

#include <string.h>
#include "small_printf.h"
#include "printf.h"
#include "LPC11xx.h"

char rfm_buffer_tx[200];
uint8_t ready_bytes = 0;
uint8_t rx_ready = 0;
volatile char *str;
volatile uint8_t n = 0;
volatile uint8_t disabled = 0;

#define min(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a < _b ? _a : _b; })

void uart_init(uint8_t clk){
	//uint8_t i;
	NVIC_DisableIRQ(UART_IRQn);

	LPC_IOCON->PIO1_6 &= ~0x07;    /*  UART I/O config */
	LPC_IOCON->PIO1_6 |= 0x01;     /* UART RXD */
	LPC_IOCON->PIO1_7 &= ~0x07;
	LPC_IOCON->PIO1_7 |= 0x01;     /* UART TXD */
	/* Enable UART clock */
	LPC_SYSCON->SYSAHBCLKCTRL |= (1<<12);
	LPC_SYSCON->UARTCLKDIV = 0x1;     /* divided by 1 */
	LPC_UART->LCR = 0x83;             /* 8 bits, no Parity, 1 Stop bit */

	switch(clk){
	case 2: //8khz
		uart_8khz();
		break;
	case 1: //12mhz
		uart_12mhz();
		break;
	case 0:
	default:
		uart_48mhz();
	}

	LPC_UART->LCR = 0x03;		/* DLAB = 0 */
	LPC_UART->FCR = 0x07;		/* Enable and reset TX and RX FIFO. */
	//while(LPC_UART->LSR & 1) i = LPC_UART->RBR;
	//	LPC_UART->IER = 0x01;		/* RDA interrupt enabled */
	LPC_UART->IER = 0x05;		/* RDA and RX status interrupts enabled */
	NVIC_EnableIRQ(UART_IRQn);
}


void uart_48mhz(){
	LPC_SYSCON->UARTCLKDIV = 0x1;
	LPC_UART->LCR |= 0x80; //enable div change
#ifndef DUMMY_PRINTF
	//for 115200 @ 48MHz
	LPC_UART->DLM = 0;
	LPC_UART->DLL = 17;
	LPC_UART->FDR = 8 | (15<<4); /* DIVADVAL=8 MULVAL=15 */
#else
	//for 460800
	LPC_UART->DLM = 0;
	LPC_UART->DLL = 5;
	LPC_UART->FDR = 3 | (10<<4); /* DIVADVAL=8 MULVAL=15 */
#endif
	disabled = 0;
	LPC_UART->LCR &= ~0x80; //disable div change
}

void uart_12mhz(){
	LPC_SYSCON->UARTCLKDIV = 0x1;
	//for 115200 @ 12MHz
	LPC_UART->LCR |= 0x80; //enable div change
	LPC_UART->DLM = 0;
	LPC_UART->DLL = 5;
	LPC_UART->FDR = 3 | (10<<4); /* DIVADVAL=5 MULVAL=8 */
	LPC_UART->LCR &= ~0x80; //disable div change
	disabled = 0;
}

void uart_8khz(){
	LPC_SYSCON->UARTCLKDIV = 0; /* disable */
	disabled = 1;
}

int uart_putchar(char p){
	//LPC_UART->FCR |= 0x2; //reset RX FIFO
	LPC_UART->THR = p;
	while (!(LPC_UART->LSR & (1<<5)));
	return 1;
}

int sprintf_putchar(char p){
	if(str == 0){
		return 0;
	}
	str[n++] = p;
}

void sprintf(char *buf, const char *format, ...){
	va_list args;
//#ifndef DUMMY_PRINTF
	va_start(args, format);
	n = 0;
	str = buf;
	printf_format(sprintf_putchar, format, args);
	str[n] = 0x00;
	str = 0;
	va_end(args);
//#endif
}


void printf(const char *format, ...){
	va_list args;
	if(disabled) return;
#ifndef DUMMY_PRINTF
	va_start(args, format);
	printf_format(&uart_putchar, format, args);
	va_end(args);
#endif
}

uint8_t atoi(uint8_t *from_str, uint16_t *to_num){
	uint16_t intval = 0;
	uint8_t ret = 0;
	while(*from_str > 47 && *from_str < 58){ //ACSII digits
		intval *= 10;
		intval += (*from_str - 48);
		ret = 1;
		from_str++;
	}
	*to_num = intval;
	return ret;
}



uint8_t uart_rx_ready(){
	return ready_bytes;
}

uint8_t uart_rx(uint8_t *buf, uint8_t maxlen){
	if(!ready_bytes)
		return 0;
	uint8_t ret = min(ready_bytes, maxlen);
	memcpy(buf, rfm_buffer_tx, ret);
	memmove(rfm_buffer_tx, (rfm_buffer_tx+ret), ready_bytes-ret);
	ready_bytes -= ret;
	return ret;
}

void UART_IRQHandler(void){
	uint8_t iir, lsr;
	__disable_irq();
	iir = LPC_UART->IIR;
	iir >>= 1;
	iir &= 0x07;
	if(iir == 0x03){ //Line status
		lsr = LPC_UART->LSR;
		if(lsr&1){ //data received
			rfm_buffer_tx[(ready_bytes++)%200] = LPC_UART->RBR;
		}else if(lsr&(1 << 4)){ //break interrupt
			rx_ready = ready_bytes;
		}else{
			lsr = LPC_UART->RBR; //dummy read
		}
	}else if(iir == 0x02){ //data received
		rfm_buffer_tx[(ready_bytes++)%200] = LPC_UART->RBR;
	}else if(iir == 0x06){ //CTI (data received)
		rfm_buffer_tx[(ready_bytes++)%200] = LPC_UART->RBR;
	}else{
		lsr = LPC_UART->RBR; //dummy read
	}
	__enable_irq();
}

void printf_test(){
#ifndef DUMMY_PRINTF
	printi(uart_putchar, 145, 10, 0, 3, 0, 'a');
	prints(uart_putchar, "Helloworld\r\n", 12, 0);
#endif
}
