/*
 * spi_nrf.h
 *
 *  Created on: 28.11.2013
 *      Author: Admin
 */

#ifndef SPI_NRF_H_
#define SPI_NRF_H_
#include <stdint.h>

/* SPI
 * SPI communication module wrapper. Used to communicate with RFM73 RF module.
 */

#ifndef __SSP_COMMON__
#define __SSP_COMMON__
#define FIFOSIZE		8
/* SSP Status register */
#define SSPSR_TFE       (0x1<<0)
#define SSPSR_TNF       (0x1<<1)
#define SSPSR_RNE       (0x1<<2)
#define SSPSR_RFF       (0x1<<3)
#define SSPSR_BSY       (0x1<<4)

/* SSP CR0 register */
#define SSPCR0_DSS      (0x1<<0)
#define SSPCR0_FRF      (0x1<<4)
#define SSPCR0_SPO      (0x1<<6)
#define SSPCR0_SPH      (0x1<<7)
#define SSPCR0_SCR      (0x1<<8)

/* SSP CR1 register */
#define SSPCR1_LBM      (0x1<<0)
#define SSPCR1_SSE      (0x1<<1)
#define SSPCR1_MS       (0x1<<2)
#define SSPCR1_SOD      (0x1<<3)

/* SSP Interrupt Mask Set/Clear register */
#define SSPIMSC_RORIM   (0x1<<0)
#define SSPIMSC_RTIM    (0x1<<1)
#define SSPIMSC_RXIM    (0x1<<2)
#define SSPIMSC_TXIM    (0x1<<3)

/* SSP0 Interrupt Status register */
#define SSPRIS_RORRIS   (0x1<<0)
#define SSPRIS_RTRIS    (0x1<<1)
#define SSPRIS_RXRIS    (0x1<<2)
#define SSPRIS_TXRIS    (0x1<<3)

/* SSP0 Masked Interrupt register */
#define SSPMIS_RORMIS   (0x1<<0)
#define SSPMIS_RTMIS    (0x1<<1)
#define SSPMIS_RXMIS    (0x1<<2)
#define SSPMIS_TXMIS    (0x1<<3)

/* SSP0 Interrupt clear register */
#define SSPICR_RORIC    (0x1<<0)
#define SSPICR_RTIC     (0x1<<1)

/* ATMEL SEEPROM command set */
#define WREN		0x06		/* MSB A8 is set to 0, simplifying test */
#define WRDI		0x04
#define RDSR		0x05
#define WRSR		0x01
#define READ		0x03
#define WRITE		0x02

/* RDSR status bit definition */
#define RDSR_RDY	0x01
#define RDSR_WEN	0x02
#endif /* __SSP_COMMON__ */

// SPI Init function. Call before use of SPI
void nrfspi_init(void);

// Makes byte transfer over SPI
// Args:
//	txbyte 		- byte to transfer (set 0xFF when nothing to send)
// Returns: received byte
uint8_t nrfspi_txrx(uint8_t txbyte);

// Asserts CS pin
void nrfspi_select(void);

// Deasserts CS pin
void nrfspi_release(void);

// Sets CE pin to high (RFM73 relative)
void nrf_start(void);

// Sets CE pin to low (RFM73 relative)
void nrf_stop(void);

#endif /* SPI_NRF_H_ */
