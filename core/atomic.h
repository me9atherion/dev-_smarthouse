/* Атомарные операции */

#ifndef ATOMIC_H_
#define ATOMIC_H_

#include "LPC11xx.h"


inline static int _irqDis(void)
{
    __ASM volatile ("cpsid i" : : : "memory");
    return 1;
}

inline static int _irqEn(void)
{
    __ASM volatile ("cpsie i" : : : "memory");
    return 0;
}

/* Принудительное разрешение прерываний при выходе
 * из блока */
//main()
//{
//    ATOMIC_BLOCK_FORCEON
//    {
//        do_something();
//    }
//}
#define ATOMIC_BLOCK_FORCEON \
    for(int flag = _irqDis();\
        flag;\
        flag = _irqEn())



inline static int _iDisGetPrimask(void)
{
    int result;
    __ASM volatile ("MRS %0, primask" : "=r" (result) );
    __ASM volatile ("cpsid i" : : : "memory");
    return result;
}

inline static int _iSetPrimask(int priMask)
{
    __ASM volatile ("MSR primask, %0" : : "r" (priMask) : "memory");
    return 0;
}


/* При выходе из блока состояние прерываний восстанавливается */
//main()
//{
//    ATOMIC_BLOCK_RESTORATE
//    {
//        do_something();
//    }
//}
#define ATOMIC_BLOCK_RESTORATE \
     for(int mask = _iDisGetPrimask(), flag = 1;\
         flag;\
         flag = _iSetPrimask(mask))


#endif /* ATOMIC_H_ */
