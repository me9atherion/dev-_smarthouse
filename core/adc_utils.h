/*
 * adc_utils.h
 *
 *  Created on: 14.04.2014
 *      Author: Admin
 */

#ifndef ADC_UTILS_H_
#define ADC_UTILS_H_
#include <stdint.h>

/* ADC Utils
 * Provides rough temperature and supply voltage value
 * for self monitoring purposes.
 */

// Makes ADC init and after deepsleep start
void adc_init(void);

// Returns temperature value in degs of Celsium
uint16_t adc_temp(void);

//Returns battery state in value: 100 - 3.7V or greater, 0 - 2.6V or less
uint8_t adc_vbat(void);

#endif /* ADC_UTILS_H_ */
