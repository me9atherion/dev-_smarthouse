/*
 * adc_utils.c
 *
 *  Created on: 14.04.2014
 *      Author: Admin
 */
#include "LPC11xx.h"
#include "adc.h"
#include "adc_utils.h"

uint16_t _vbatt, vbatt;

void correct_vbatt(){
	/*uint16_t */_vbatt = ADCRead(7); //measure VDD
	if (_vbatt>862)
		vbatt=37;
//	else if (_vbatt>=862 && _vbatt<861)
//		vbatt=41;
//	else if (_vbatt>=965 && _vbatt<980)
//		vbatt=40;
//	else if (_vbatt>=957 && _vbatt<965)
//		vbatt=39;
//	else if (_vbatt>=950 && _vbatt<957)
//		vbatt=38;
//	else if (_vbatt>=947 && _vbatt<950)
//		vbatt=37;
	else if (_vbatt>=860 && _vbatt<861)
		vbatt=36;
	else if (_vbatt>=856 && _vbatt<860)
		vbatt=35;
	else if (_vbatt>=852 && _vbatt<856)
		vbatt=34;
	else if (_vbatt>=851 && _vbatt<852)
		vbatt=33;
	else if (_vbatt>=846 && _vbatt<851)
		vbatt=32;
	else if (_vbatt>=841 && _vbatt<846)
		vbatt=31;
	else if (_vbatt>=828 && _vbatt<841)
		vbatt=30;
	else if (_vbatt>=823 && _vbatt<828)
		vbatt=29;
	else if (_vbatt>=816 && _vbatt<823)
		vbatt=28;
	else if (_vbatt<816)
		vbatt=27;
}

//old
//void correct_vbatt(){
//	/*uint16_t */_vbatt = ADCRead(7); //measure VDD
//	if (_vbatt>=1000)
//		vbatt=42;
//	else if (_vbatt>=980 && _vbatt<1000)
//		vbatt=41;
//	else if (_vbatt>=965 && _vbatt<980)
//		vbatt=40;
//	else if (_vbatt>=957 && _vbatt<965)
//		vbatt=39;
//	else if (_vbatt>=950 && _vbatt<957)
//		vbatt=38;
//	else if (_vbatt>=947 && _vbatt<950)
//		vbatt=37;
//	else if (_vbatt>=945 && _vbatt<947)
//		vbatt=36;
//	else if (_vbatt>=942 && _vbatt<945)
//		vbatt=35;
//	else if (_vbatt>=940 && _vbatt<942)
//		vbatt=34;
//	else if (_vbatt>=938 && _vbatt<940)
//		vbatt=33;
//	else if (_vbatt>=936 && _vbatt<938)
//		vbatt=32;
//	else if (_vbatt>=934 && _vbatt<936)
//		vbatt=31;
//	else if (_vbatt>=932 && _vbatt<934)
//		vbatt=30;
//	else if (_vbatt>=929 && _vbatt<932)
//		vbatt=29;
//	else if (_vbatt>=926 && _vbatt<929)
//		vbatt=28;
//	else if (_vbatt<926)
//		vbatt=27;
//}

void adc_init(){
	ADCInit(ADC_CLK);
	//stick_add_task(correct_vbatt, 50);
}


uint16_t adc_temp(){
	adc_init();
	uint16_t value = ADCRead(0);
	correct_vbatt();
	//uart_printf("_VBATT: %u, VBATT: %u, _temp: %u,", _vbatt, vbatt, value);
	if (vbatt==42)                      // calculate temp when vbat change
		value=value/48;
	else if (vbatt==41)
		value=value/47;
	else if (vbatt==40)
		value=value/46;
	else if (vbatt>=34 && vbatt<40)
		value=value/45;
	else if (vbatt>=27 && vbatt<34)
		value=value/44;
	//uart_printf("temp: %u\r\n", value);
	return value;
}

uint8_t adc_vbat(){
	//adc_init();
	correct_vbatt();
	//uart_printf("_VBATT: %u, VBATT: %u, CALC VBAT: %u,", _vbatt, vbatt,(((vbatt-26)*10)));
	return (((vbatt-26)*10)); //Full charge 42(36) - 26 = 10 that is 100%
}
