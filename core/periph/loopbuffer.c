/*
 * loopbuffer.c
 *
 *  Created on: 26.02.2014
 *      Author: Admin
 */
#include <string.h>
#include "loopbuffer.h"
#ifdef TEST
#include <stdio.h>
#define printf printf
#else
#include "printf.h"
#endif

//tested 25_03

uint8_t rfm_buffer_tx[LOOPBUFFERS_COUNT][LOOPBUFFER_BYTESIZE];
uint8_t size[LOOPBUFFERS_COUNT];
uint16_t maxcount[LOOPBUFFERS_COUNT];

uint16_t head[LOOPBUFFERS_COUNT];
uint16_t tail[LOOPBUFFERS_COUNT];

uint8_t inited = 0;

uint8_t *buf(uint8_t num, uint16_t offset){
	return &rfm_buffer_tx[num][(offset%maxcount[num])*size[num]];
}

void lb_init(){
	if(inited)
		return;
	int i = 0;
	for(; i < LOOPBUFFERS_COUNT; i++){
		memset(rfm_buffer_tx[i], 0x00, LOOPBUFFER_BYTESIZE);
	}
	memset(size, 0x00, LOOPBUFFERS_COUNT);
	memset(maxcount, 0x00, LOOPBUFFERS_COUNT);
	memset(head, 0x00, LOOPBUFFERS_COUNT);
	memset(tail, 0x00, LOOPBUFFERS_COUNT);
	inited = 1;
}

uint8_t lb_alloc(size_t struct_size){
	uint8_t i;
//	for(i = 0; i < LOOPBUFFERS_COUNT; i++)
//		printf("%u ", size[i]);
//	printf("\r\n");
	if(struct_size > LOOPBUFFER_BYTESIZE)
		return 0xff;
	for(i = 0; i < LOOPBUFFERS_COUNT; i++){
		if(size[i] == 0)
			break;
	}
	if(i == LOOPBUFFERS_COUNT)
		return 0xfe;
	size[i] = struct_size;
	maxcount[i] = LOOPBUFFER_BYTESIZE / size[i];
	//printf("LB ALLOC: num=%u maxcount=%u size=%u Success\r\n", i, maxcount[i], size[i]);
	return i;
}

uint8_t lb_is_free(uint8_t num){
	if(num > LOOPBUFFERS_COUNT-1)
		return 0;
	return (head[num] == tail[num]);
}

uint8_t lb_is_full(uint8_t num){
	if(num > LOOPBUFFERS_COUNT-1)
		return 0;
	return (head[num] >= tail[num]+maxcount[num]);
}

uint8_t lb_put(uint8_t num, void *data){
	if(num > LOOPBUFFERS_COUNT-1 || size[num] == 0)
		return 0;
	memcpy(buf(num, head[num]), data, size[num]);
	head[num]++;
	if(head[num] > tail[num]+maxcount[num])
		tail[num]++;
	//printf("LB PUT: head=%u tail=%u Success\r\n", head[num], tail[num]);
	return 1;
}

uint8_t lb_get(uint8_t num, void *data){
	if(num > LOOPBUFFERS_COUNT-1 || size[num] == 0 || data == NULL)
		return 0xff;
	if(lb_is_free(num))
		return 0;
	//we don't check "data" pointer for proper memory, we can crash! be carefull
	memcpy(data, buf(num, tail[num]), size[num]);
	memset(buf(num, tail[num]), 0x00, size[num]);
	tail[num]++;
	//printf("LB GET: head=%u tail=%u Success\r\n", head[num], tail[num]);
	return 1;
}

uint8_t lb_free(uint8_t num){
	if(num > LOOPBUFFERS_COUNT-1)
		return 0;
	if(size[num] == 0)
		return 1;
	memset(rfm_buffer_tx[num], 0x00, LOOPBUFFER_BYTESIZE);
	size[num] = 0;
	maxcount[num] = 0;
	head[num] = 0;
	tail[num] = 0;
	return 1;
}

uint8_t lb_clean(uint8_t num){
	if(num > LOOPBUFFERS_COUNT-1 || size[num] == 0)
		return 0;
	memset(rfm_buffer_tx[num], 0x00, LOOPBUFFER_BYTESIZE);
	head[num] = 0;
	tail[num] = 0;
	return 1;
}


