/*
 * leds.c
 *
 *  Created on: 23.12.2013
 *      Author: Admin
 */

#include "LPC11xx.h"
#include "leds.h"
#include "gpio.h"
#include "pwr.h"
#include "counter.h"

#define LED_COUNT 2

#define LED1_PORT 1
#define LED0_PORT 1
#define LED1_PIN 1
#define LED0_PIN 0
/* LED1 P1.1
 * LED2 P1.0
 */

uint8_t led_ports[LED_COUNT] = {LED0_PORT, LED1_PORT};
uint8_t led_pins[LED_COUNT] = {LED0_PIN, LED1_PIN};
uint32_t led_blinks[LED_COUNT] = {0};

void led_poll(){
	for(uint8_t i = 0; i < LED_COUNT; i++){
		if(!led_blinks[i])
			continue;
		if(count_1khz() - led_blinks[i] > 100){
			GPIOToggle(led_ports[i], led_pins[i]);
			led_blinks[i] = 0;
		}
	}
}

void led_init(){
	GPIOInit();
	LPC_IOCON->R_PIO1_0  &= ~0x07;
	LPC_IOCON->R_PIO1_0  |= 0x01;
	LPC_IOCON->R_PIO1_1  &= ~0x07;
	LPC_IOCON->R_PIO1_1  |= 0x01;
	//LED1 OFF
	for(uint8_t i = 0; i < LED_COUNT; i++){
		GPIOSetDir(led_ports[i], led_pins[i], 1);
		GPIOSetValue(led_ports[i], led_pins[i], 0);
	}
	//LED0 Blink
	led_on(0);
	asm_wait(20);
	led_off(0);
	asm_wait(20);
	led_on(0);
	asm_wait(20);
	led_off(0);
}

void led_on(leds_t led){
	GPIOSetValue(led_ports[led], led_pins[led], 1);
}

void led_off(leds_t led){
	GPIOSetValue(led_ports[led], led_pins[led], 0);
}

void led_blink(leds_t led){
	GPIOToggle(led_ports[led], led_pins[led]);
	led_blinks[led] = count_1khz();
}
